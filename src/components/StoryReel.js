import React from 'react';
import "../css/StoryReel.css";
import Story from "../components/Story"

function StoryReel() {
    return (
        <div className="storyreel">
           <Story   image="https://static1.bigstockphoto.com/1/3/2/large1500/231780889.jpg"
                    profileSrc="https://www.happyhelps.fr/src/uploads/members/341574426170.jpeg" 
                    title="Johnny stash"
            />
            <Story   image="https://cdn.wallpapersafari.com/86/35/62hzN8.jpg"
                    profileSrc="https://i.pinimg.com/originals/16/63/67/166367bd3aa10af642a66bf00b19ca16.jpg" 
                    title="Rebecca Sanders"
            />   
            <Story   image="https://i.pinimg.com/originals/f8/1c/88/f81c8848bdb11187cdfd2254d0ea8f7f.jpg"
                    profileSrc="https://i.kym-cdn.com/photos/images/original/000/934/730/6f5.png" 
                    title="Ali Gun G"
            />   
            <Story   image="https://static.vecteezy.com/system/resources/previews/000/366/560/non_2x/retro-background-vector.jpg"
                    profileSrc="https://i.pinimg.com/originals/6b/f6/c6/6bf6c606d558e42c14848ea8aaefbce1.jpg" 
                    title="Milie Drop"
            />   
            <Story   image="https://lenergeek.com/wp-content/uploads/2019/11/france-image-marche-energie-degrade-LEnergeek.jpg"
                    profileSrc="https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80" 
                    title="Arthur Bolder"
            />              
                     
        </div>
    )
}

export default StoryReel
