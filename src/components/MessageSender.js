import { Avatar } from '@material-ui/core';
import React, {useState} from 'react'
import "../css/MessageSender.css";
import VideocamIcon from '@material-ui/icons/Videocam';
import PhotoLibraryIcon from '@material-ui/icons/PhotoLibrary';
import EmojiEmotionsIcon from '@material-ui/icons/EmojiEmotions';
import {useStateValue} from '../provider/StateProvider';
import db from '../firebase';
import firebase from 'firebase'


function MessageSender() {

    const [ {user}, dispatch ] = useStateValue();

    const [ input, setInput ] = useState('');
    const [ imageURL, setImageURL] = useState(''); 
    

    const handleSubmit = (e) => {
        e.preventDefault();

        db.collection('posts').add({
            message: input, 
            timestamp: firebase.firestore.FieldValue.serverTimestamp(),
            profilPic : user.photoURL,
            username: user.displayName,
            image: imageURL,
        })

        // some logic to save stuff in bd


        setInput('');
        setImageURL('');

    }

    return (
        <div className="messagesender">
            <div className="messagesender__top">
                <Avatar />
                <form>
                    <input  value={input}
                            onChange={e => setInput(e.target.value)}
                            className="messagesender__input" 
                            placeholder={"Whats on your mind ?"}/>

                    <input 
                            placeholder={"Image URL (optionnal)"}
                            value={imageURL}
                            onChange={e => setImageURL(e.target.value)} />
                    <button onClick={handleSubmit} type="submit">
                        Hidden Submit
                    </button>
                </form>

            </div>
            <div className="messagesender__bottom">
               
                <div className="messagesender__option">
                    <VideocamIcon style={{color:"red"}} />
                    <h3>Live Video</h3>
                </div>
                <div className="messagesender__option">
                    <PhotoLibraryIcon style={{color:"green"}} />
                    <h3>Photo/Video</h3>
                </div>
                <div className="messagesender__option">
                    <EmojiEmotionsIcon style={{color:"orange"}} />
                    <h3>Feeling/acitvity</h3>
                </div>
                
                
            </div>
            
        </div>
    )
}

export default MessageSender
