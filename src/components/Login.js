import { Button } from '@material-ui/core';
import React from 'react';
import "../css/Login.css";
import { auth, provider } from "../firebase";
import { actionTypes } from '../provider/Reducer';
import { useStateValue } from '../provider/StateProvider';



function Login() {

    const [ state, dispatch ] = useStateValue();

    const signIn = () => {
        auth.signInWithPopup(provider)
            .then((result) => {
                dispatch({
                    type: actionTypes.SET_USER,
                    user: result.user,
                })
            })
        .catch((error) => alert(error.message));        
    };

    return (
        <div className="login">
            <div className="login__logo">
                <img src="https://img.generation-nt.com/facebook_04B0032001662445.png" alt="" />
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQWFSjxox6qeWHlMe6_lOGAgOuZn-2VX9Hrag&usqp=CAU" alt="" style={{ width:"200px", height:"100px", margin:"0 auto" }}/>
            </div>   
            <Button type="submit" onClick={signIn} > Sign In </Button>
        </div>
    )
}

export default Login
