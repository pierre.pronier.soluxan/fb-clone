import firebase from "firebase"

const firebaseConfig = {
    apiKey: "AIzaSyAJAtzfI5xP_aL06yGOrciN4H8BUw25VUs",
    authDomain: "fb-clone-95915.firebaseapp.com",
    databaseURL: "https://fb-clone-95915.firebaseio.com",
    projectId: "fb-clone-95915",
    storageBucket: "fb-clone-95915.appspot.com",
    messagingSenderId: "504811376407",
    appId: "1:504811376407:web:2b48f4192324f10a45b152"
  };

  const firebaseapp = firebase.initializeApp(firebaseConfig);
  const db = firebaseapp.firestore();
  const auth = firebase.auth();
  const provider = new firebase.auth.GoogleAuthProvider();

  export {auth , provider};
  export default db;