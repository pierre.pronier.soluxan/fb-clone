import './App.css';
import Header from "./components/Header";
import SideBar from "./components/SideBar";
import Feed from "./components/Feed";
import Widget from "./components/Widget";
import Login from "./components/Login";
import { useStateValue } from './provider/StateProvider';


function App() {

  const [ { user }, dispatch ] = useStateValue(); 

  return ( 

    <div className="App">
      {!user ? (
        <Login />
      ) : (
        <>
          <Header />
          <div className="app_body">
            <SideBar />
            <Feed />
            <Widget />          
          </div> 
        </>
      )

      };
      
      
          
    </div>   
  );
}

export default App;

// metre sous la balise body
// <!-- The core Firebase JS SDK is always required and must be listed first -->
//<script src="/__/firebase/8.0.1/firebase-app.js"></script>

//<!-- TODO: Add SDKs for Firebase products that you want to use
//     https://firebase.google.com/docs/web/setup#available-libraries -->

//<!-- Initialize Firebase 
//<script src="/__/firebase/init.js"></script>
